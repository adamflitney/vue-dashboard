const axios = require("axios");

const WeatherAPI = {
  getForecast() {
    var request =
      "https://api.darksky.net/forecast/" +
      process.env.VUE_APP_WEATHER_API_KEY +
      "/" +
      "51.266541,-1.092396" +
      "?units=uk2";

    return axios.get(request);
  }
};
export default WeatherAPI;
