import Vue from "vue";
import Vuex from "vuex";

import WeatherModule from "./WeatherModule";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    WeatherModule
  }
});
