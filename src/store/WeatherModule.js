import WeatherAPI from "@/api/WeatherAPI";

const state = {
  forecast: []
};

const getters = {
  currentForecast: state => {
    return state.forecast;
  }
};
const mutations = {
  updateForecast(state, payload) {
    state.forecast = payload;
  }
};

const actions = {
  loadForecast(context) {
    return new Promise(resolve => {
      WeatherAPI.getForecast().then(response => {
        console.log(response.data);
        context.commit("updateForecast", response.data);
        resolve();
      });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
